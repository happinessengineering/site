# EXECUTIVE STATEMENT

---

Interested in roles within high growth, creative and innovative organizations or teams to which I can provide leadership skills, creative and technical guidance and direct contributions to meaningful objectives.

I have a rich history spanning multiple industries and have worked both behind the scenes and directly with customers and clients to identify and solve problems, deliver best-in-class solutions and improve the overall effectiveness and efficiency of both the organization and the customer.

I have worked extensively in both the creative and technology fields with some of the best teams and most highly regarded individuals. I am comfortable with the unknown and the unexpected, thriving in conditions where excellence is required but compromise is not. I am adept at managing scarce resources and delivering on expectations even when execution does not run according to plan.

I am a voracious learner and an avid teacher, with a strong appetite for finding, interpreting and sharing new knowledge. I expend great energy in lifting team members beyond their current state, making the team more effective, efficient and generally sustainable. 

I believe strongly in the power of great communication and am meticulous in my own documentation, whether for internal purposes or external consumption.

My ideal positions see me partnered with other like minded individuals, as a driver of change and dogged implementation specialist. Although I bring to the table many of my own valued skills and traits, I most look forward to bettering myself by working with smart, ambitious and action-focused collaborators. I am confident in what I know, but know there’s so much left to learn.


# PROFILE

---

Extensive experience and demonstrated success as a hands-on leader within high volume/high growth creative and technology organizations, employing modern management and supervisory ideologies, approaches and responsibilities, including:

* Day-to-day and executive-level technology and creative project management utilizing both structured (Prince2, Six Sigma) and agile (Scrum, XP, Kanban) methodologies.
Establishment of long-term organizational vision (financial, staffing and creative) and creation of measurable goals for success.
* Optimization of existing revenue streams.
* Market analysis and identification of potential new revenue streams.
* Development and integration of organizational best practices and standardized workflows.
* Emerging technology research and subsequent integration of cutting edge tools, methods and techniques.
* Resource allocation and scheduling.
* Financial analysis, reporting, monitoring and planning.
* Internal and external client and business development.
* Conflict management and resolution.

Certified professional information technology practitioner with extensive experience in networked systems and infrastructures for high traffic, creative environments deployed in traditional, cloud and hybrid configurations: 

* Data and file management - storage, optimization, archival, backup and restore.
* Networked, high-capacity storage - Fibre, copper or cloud based. SAN, NAS, iSCSI, AWS S3, EBS, EFS, Glacier and Storage Gateway.
* Local, high-performance storage mechanisms for creative and other low-latency applications.
* Advanced network design, installation, maintenance and support.
* Comprehensive security maintenance - intrusion prevention and detection - both in the cloud (AWS security groups, NACLs, policies, WAF, Hashicorp Vault, AWS Secrets Manager, etc) and on-premises.
* DevOps methodologies and frameworks including continuous delivery/integration, testing and deployment methods. 
* Database management with both SQL and NoSQL systems.
* API development, deployment and support.
* Mobile application infrastructure - authentication, security and content delivery.
* Web server design - Apache, Nginx and full LAMP configurations.
* DNS Management - private BIND implementations and AWS Route53.
* Peer-to-peer and site-to-site private connectivity via both hardware and software VPN solutions.
* Hardware switching, firewall and load balancer deployment and support - Dell, Cisco, HP, Juniper, Foundry.
* Custom installation/kernel development, deployment and support - OSX, Windows, Linux.
* On-premises and cloud-based development, virtualization, containerization and deployment methodologies and platforms - VMWare, AWS (including EC2, CodeDeploy, CodePipeline, CloudFormation, OpsWorks and ECS), Vagrant, Docker, Chef, Saltstack, Gitlab/Gitlab CI, Jenkins, Spinnaker, TravisCI.
* AWS and cloud infrastructure-as-code tools: Raw CloudFormation, Hashicorp Terraform.
* Kubernetes management - deployment, maintenance and support.
* Monitoring and reporting: New Relic, CloudWatch, CloudTrail, Nagios, Sumo Logic, Splunk, SysDig, DataDog, Prometheus, Grafana.
* Directory services and user control: Web Identity federation, AWS IAM, Active Directory, LDAP.
* Version control and code management: SVN, Git (Github/Gitlab).
* Environments: Windows (XP, Vista, 7x, 8x, 10, Server), Macintosh (OS 9/OS X), Unix (Linux, Ubuntu/CentOS/RHEL, Solaris).
* Languages: HTML/HTML5, CSS/CSS3, PHP, Java/JavaScript, Python/Flask, Bash shell script, Smalltalk, Golang, C/C++.

Creative production and post-production workflows:

* Microphone technique (vocal, instrumental, field), tracking, spotting, cue sheets, documentation.
* Dialog, foley, effects, sound design/editing, predub and stem creation and mixing.
* Post/Production supervision - planning, resource allocation, tracking/reconciliation, and delivery.
* Tape formats, dubbing, video capture and editing (digital and analog formats).
* Asset creation/delivery for albums, commercials, short films, feature films and interactive media.
* Game Middleware: FMOD, wWise, xACT, and proprietary systems.
* Environments: Avid Pro Tools/HD, Apple Logic Pro, Symbolic Sound Kyma X, Max/MSP, Pure Data, Steinberg Cubase and Nuendo, Apple Final Cut, Avid MediaComposer/Editing suite.

# PROFESSIONAL EXPERIENCE

---

## Boutique Technical Due Diligence group // West Coast, USA
#### Senior Consultant and Engagement Manager // 2021 - Present

* Senior consultant and engagement manager for a boutique technical due diligence and technology assessment group, working in the context of mergers, acquisitions and internal auditing.
* Provide comprehensive, confidential and unbiased analysis of software, hardware and machine learning and artificial intelligence solutions.
* Evaluate industry best practice compliance in categories such as code quality, security, privacy, open source usage and many more.
* Lead engagements and act as point-of-contact and liason between all parties.
* Ensure on-time and on-budget delivery of all scoped items.
* Clients and targets include the Fortune 10 with transactions greater than $1 billion USD.

## Venture backed SaaS startup // Midwest, USA
#### Senior DevOps Engineer // 2016-2021


* Senior DevOps Engineer for an award-winning, venture funded (series C, over $200 million USD secured) startup providing scheduling solutions for hundreds of thousands of businesses and millions of workers.
* Drive infrastructure design, implementation, performance and security across the entire organization.
* Evangelize cutting edge solutions using industry best practices and tools.
* Key team member in migration to Amazon Web Services, adoption of Serverless workflows, integration of a comprehensive security protocol,transition to Kubernetes and end-to-end development pipelines.
* Authored and ultimately responsible for the support of several significant internal tools, used across departments.
* Serve as the main point of contact for infrastructure and operations security.
* Manage security compliance, lead engagements with security vendors and implement cutting edge security tooling and procedures like Hashicorp’s Vault and Public Key Infrastructure.
* Primary platforms: Git (Github/Gitlab), Amazon Web Services, Microsoft Azure, Kubernetes, Serverless (AWS Lambda), Hashicorp Terraform, Docker.
* Primary languages: Python, Golang, PHP, Javascript/ReactJS, HTML/HTML5, CSS/CSS3, Bash shell script.


## Freelance // West Coast, USA
#### Creative Solutions Architect and Cloud Engineer // 2016-Current 


* Freelance creative solutions architect and hands-on cloud engineer with a focused interest in creative infrastructure for high- traffic,fast-paced, highly scalable environments.
* Provide subject matter expertise for startups and organizations in such areas as “indie” game development, data center migration and SaaS platform deployment.
* Perform audits of existing infrastructure and create architecture diagrams, automation scripts and maintenance/support procedures for new implementations.
* Primary platforms: Amazon Web Services, Kubernetes, Serverless, Docker.
* Primary languages: Python, Golang, Java, PHP, Javascript/ReactJS, HTML/HTML5, CSS/CSS3, Bash shell script.


## Freelance // West Coast, USA
#### Sound Designer, Composer and Re-recording mixer // 2009-Current


* Freelance sound designer, composer and re-recording mixer specializing in original audio content for television, film, video games and all forms of digital and online interactive media.
* Provide vision for creative output and research and implement infrastructure decisions for both hardware and software.
* Collaborate with a diverse set of stakeholders to design custom audio experiences - often in multi-mode contexts, such as in support of film scripts or commercial brand messaging.
* Create engaging, dynamic audio content – including sound design, music and mix – for all projects across all mediums.


## Government grant funded media startup // UK/West Coast, USA
#### Director of Technology and Business Operations // 2015-2016 


* Director of Technology and Business Operations for an innovative, boutique entertainment and technology startup. 
* Led product development through ideation, prototype, MVP and MSP - both front and back end.
* Created operational and technology milestones, including actionable documentation outlining efficient models for their achievement.
* Designed technology best practices and standard production protocols and ensured their implementation across all projects and with all team members.
* Evaluated cutting-edge solutions for the entire portfolio, from application infrastructure to API usage to virtual reality post-production workflows, and provided recommendations and implementation processes when appropriate.
* Researched, designed and implemented organization-wide process changes, such as document sharing, on boarding procedures and third-party application usage, to drive dynamic, efficient and rapid growth.
* Primary platforms: Amazon Web Services, Docker.
* Primary languages: C++, Objective-C, Swift, Python, HTML/HTML5, CSS/CSS3, JavaScript, Bash shell script.


## Bootstrapped technology consulting startup // Midwest, USA
#### Head of Production // 2013-2015


* Head of Production for a creative technology startup and Amazon Web Services Consulting  Partner with a focus on building highly available, highly resilient, highly secure and highly scalable architectures and infrastructures in the cloud.
* Acted as solutions architect, project manager and hands-on DevOps engineering support (infrastructure and architecture design and implementation) for clients and lead engineers.
* Managed the short and long-term operation of the firm, including but not limited to the setting of financial goals and projections, staffing decisions, project portfolio management and business opportunities.
* Designed and implemented business strategies, such as high visibility partner relationships, to maximize both organizational growth and revenue potential.
* Supported business development initiatives by creating, nurturing and maintaining existing and potential client relationships. 
* Managed the day-to-day operations of all internal and client-facing projects, including but not limited to writing and proofing scopes of work,financial reporting and monitoring, project resource scheduling and allocation and ensuring on-time and on-budget completion.
* Clients included a well known online retailer, a Fortune 50 pharmaceutical company, a leading sports media company, and a top Ivy League university.
* Primary platforms: Amazon Web Services.
* Primary languages: Python, Java, Javascript.


## Top ranked media and arts university // Midwest, USA
#### Adjunct Professor // 2011-2015


* Adjunct professor for a top ranked media and arts university.
* Responsible for creating and delivering two comprehensive 16 week courses - two of the most popular and well attended on campus - for students across multiple disciplines.
* Ensure student success via consistent and proactive guidance and outreach.
* Regularly participate in department planning and strategy sessions.
* Regularly interface with the Department Leads for status updates, concerns and resource requests.
* Classes:
    * Audio for Picture I
        * Required entry level technical class for undergraduate students in the post production audio and film curriculums.
        * Content includes common professional vocabulary, hardware, software and studio workflows.
        * Students evaluated via a combination of exam and project work.
    * Aesthetics of Film Sound
        * Elective for undergraduate students in any discipline interested in cultivating a more complete understanding of the mechanics of film, in particular the generation of meaning through sound.
        * Content includes history, definitions, examples and analysis of film and film sound, plus deep discussion of the associated philosophical and psychological tenets.
        * Students evaluated via ongoing creative writing assignments and project work (e.g. film analysis).


## Award winning "Fortune 500" global advertising agency // Midwest, USA
#### Post Production Supervisor // 2011-2013


* Managed all aspects of the in-house broadcast production/post-production facility for a full-service global advertising agency,specializing in branding, strategic planning, creative development and promotions.
* Worked directly with all disciplines (leadership, production, account management, business affairs and creative) to ensure timely, cost-efficient and high quality creative output for internal, corporate, national and global projects.
* Co-led the design of a new facility consisting of 2 audio rooms, 1 vocal booth, 2 CG/retouching suites, 3 editorial suites, a modern mount room and open workstation space to accommodate up to 30 freelancers.
* Designed and implemented the first and only copper-based 10GigE network within the Omnicom family.
* Modernized final asset archival and retrieval workflow, resulting in new, efficient revenue streams for both national and global clients.
* Designed and implemented proprietary tools to automate repetitive/unsupervised tasks.
* Solely responsible for day-to-day operations of multiple discipline facility for projects including production (shoot), editorial, 2D/ 2.5D/3D animation, motion design/graphics, audio recording, sound design, original music composition and mix.
* Solely responsible for facility's financial tracking, scheduling, resourcing, maintenance, support and end-to-end project management.
* Continually evaluated new and emerging technology to fit the needs of the expanding user base.
* Rapidly expanded available services to include high-definition capabilities, TVC quality control and distribution, 3D animation/modeling,original music composition, video storyboarding, directing and production, and national and global adaptations/reapplications.
* Led successful transition from legacy tape to modern file-based workflow.
* Increased revenue by over 100% in under 20 months.
* Primary environments: Apple Mac OSX 10.6/10.7/10.8 (server and desktop).
* Supported hardware: Apple workstations, Cisco 4000 and 5000 series, HP 8200 Series.
* Primary languages: HTML, PHP, Python, Shell scripting.


## Specialized servicecs, Top ranked university // Midwest, USA
#### Network Analyst and Coordinator of Network and Technology Operations // 2009-2011


* Network analyst for a specialized internal group of a major midwest university.
* Responsible for the design, day-to-day health, maintenance and future growth of the university's networks, servers and applications.
* Point of contact with vendors for procurement negotiation, troubleshooting and supervision of maintenance agreements and site-licenses of hardware and software.
* Implemented, maintained and provided network connections and facilities for enterprise network solutions (utilizing Cisco, Foundry and HP switches and routers), wireless network (utilizing Cisco access points and controllers), and workstation networks.
* Provided 24/7 on-call triage and engineering support for mission-critical applications, services and infrastructure.
* Coordinated work with other AITS groups both informally via personal communication and formally via standardized control change procedures.
* Assisted in the research and design of the administrative network’s core infrastructure refresh, replacing legacy hardware and implementing a simplified high-speed network designed to increase performance and reliability to meet information processing and traffic needs while ensuring future scalability.
* Continually evaluated emerging technology to fit the needs of the university administration’s user base.
* Worked directly with security staff to actively evaluate, monitor and control current and future security threats.
* Primary environments: Linux, Microsoft Windows, Solaris.


## Award winning boutique interactive developer // Midwest, USA
#### Audio Specialist // 2008


* Audio specialist for a third-party developer of interactive software titles – consumer games and simulations.
* Created and implemented original audio content and tooling for all projects on a variety of platforms.
* Assisted in the design and planning for a major update to the organization’s audio post production hardware, software and processes.
* Platforms: Sony PlayStation Portable (PSP), Sony PlayStation 2, Nintendo Wii, Microsoft Xbox360, PC. 
* Primary languages: C++, Lua.
* Tools: Steinberg Cubase 4.x, Sony Soundforge 8.x/9.x, Sony Vegas, Adobe Premiere, proprietary software.


## Award wining boutique audio post-production company // West Coast, USA
#### Staff Sound Designer, Composer, Music Editor // 2005-2008 


* Staff sound designer, composer and music editor for an award winning, bicoastal audio post-production company.
* Created award-winning original audio content for a vast array of high profile clients across multiple mediums, including television, film, video games and other forms of interactive/digital distribution.
* Lead client work-sessions to address feedback, revisions and final output.
* Researched and implemented new production tools and techniques, such as standardized field recording kits and spatial mixing tools.
* Clients included top Hollywood studios and major national and international brands.


## Award wining boutique audio post-production company // West Coast, USA
#### Systems and Network Administrator // 2004-2005 


* Systems and network administrator for an award winning, bicoastal audio post-production company. 
* Maintained and developed machine and network infrastructure. 
* Communicated with production staff to ensure project goals were met creatively, effectively and in a timely fashion. 
* Supported new and ongoing projects with creative audio and technology solutions, such as shared storage and offsite/client collaboration.
* Provided all levels of technical support for clients and staff.
* Researched and developed scalable solutions for new media creation, usage and delivery, such as Fibre Channel and iSCSI based storage arrays.
* Modernized all aspects of machine and network infrastructure - servers and clients - including CISCO gigabit switching, VPN access and security enforcement, implemented cohesive iSCSI based storage, backup and restore policy, designed and installed network topography and functions for new physical locations (wiring, switching, connectivity and policies), created and maintained company website.
* Primary environments: OS X (desktop and server), Windows (XP, Vista).


## Specialized services group, Top ranked university, Computer science department // Midwest, USA
#### Senior System/Network Engineer // 2000-2003 
#### Student


* Senior System/Network Engineer for a specialized group of student system administrators within a university's computer science department.
* Led a small team of network administrators in providing support for all aspects of the sophisticated multi-platform departmental network - consisting of over 1000 clients and 500 servers.
* Designed, installed and maintained large-scale network topographies and functions.
* Provided solutions for future development and research.
* Developed and implemented network policies.
* Built and maintained web resources.
* Acted as liaison between support and departmental staff and administered 24 hour technical support.
* Modernized and automated large-scale software package (RPM) upgrade procedure.
* Modernized trouble ticket submission system via custom Perl-based interfaces.
* Developed and implemented the department's first LDAP infrastructure.
* Primary environments: Solaris, Linux.
* Primary languages: Perl, shell script.


## Government funded research group // Midwest, USA
#### Senior System/Network Engineer // 1998-2000
#### Student


* Network Engineer for a small, Navy-funded research group.
* Administered all aspects of a sophisticated, multi-domain Windows-NT network.
* Maintained scalable images for multicast distribution.
* Administered Exchange 5.5 server and clients.
* Primary environments: Windows NT.


# CERTIFICATIONS

---

#### Amazon Web Services

* Certified Solutions Architect - Professional

#### Amazon Web Services

* Certified Cloud Practitioner

#### Amazon Web Services

* Certified Developer - Associate

#### Amazon Web Services

* Certified SysOps Administrator - Associate

#### Amazon Web Services

* Certified Solutions Architect - Associate


# EDUCATION

---

#### Top ranked university // Midwest, USA

* Bachelor of Arts // 1998-2003

#### Top ranked media and arts university // East Coast, USA

* Associate of Science // 2003-2004
