// exported interfaces

export interface IProject {
    title: string;
    description: string;
    imageUrl: string;
    content: string;
    linkUrl: string;
}