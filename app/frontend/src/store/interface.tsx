// Defining your own state and associated actions is required
// example:
//
// type appGlobalState = {
//    value: string;
// };
//

export type appGlobalState = {
    mainBackgroundImageUrl: string;
    unsplashSourceUrl: string;
    drawerOpen: boolean;
    loading: boolean;
    staticBackgroundImageUrls: string[];
};

