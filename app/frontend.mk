
.PHONY: default
default:

PWD=$(shell pwd)

ifndef DEPLOY_ENV
  DEPLOY_ENV := development
endif

frontend-deps: ## install the frontend depedencies
frontend-deps:
	cd app/frontend; \
	  npm install

frontend-start-dev: ## start the development server
frontend-start-dev:
	cd app/frontend; \
	  npm start

frontend-build: ## build the frontend
frontend-build:
	cd app/frontend; \
	  npm run build

frontend-deploy: ## deploy the frontend
frontend-deploy:
	cd app/frontend/build; \
	  aws s3 sync --delete . s3://happiness.engineering/ ;\
	  pandoc ./cv.md -t context -o ./cv.pdf ;\
	  aws s3 cp cv.pdf s3://happiness.engineering/cv.pdf