include app/*.mk
include terraform/*.mk

PWD=$(shell pwd)

ifndef DEPLOY_ENV
  DEPLOY_ENV := development
endif

.PHONY: default
default: help

# internal recipes

check:
	@if [ ! -z $(FORCE) ]; then echo "Forcing..."; exit 0; fi; \
		echo "Are you sure? [NO/yes]:"; \
		read answer; \
		if [ "$$answer" != "yes" ]; then echo "Must answer 'yes' to proceed."; exit 1; fi

env-check:
	@echo "Using DEPLOY_ENV: $(DEPLOY_ENV)." ;\
		echo "Sleeping 2s before continuing... Control-C now to abort!" ;\
		sleep 2; \

# from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## print this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort \
		| sed 's/^.*\/\(.*\)/\1/' \
		| awk 'BEGIN {FS = ":[^:]*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

