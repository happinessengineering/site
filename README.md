[https://happiness.engineering](https://happiness.engineering)

Frontend built with CRA and Typescript. Dynamic background courtesy of [Unsplash](https://unsplash.com).

Markdown-to-PDF conversion with [pandoc](https://pandoc.org/).

Deployed to AWS S3 fronted by Cloudfront and Route53. All Infrastructure as Code resources defined and maintained by [Terraform](https://www.terraform.io/).

Project interface provided by [`make`](https://en.wikipedia.org/wiki/Make_(software))

Development quick start:

`$ make frontend-start-dev`

Production build and deploy:

`$ make tf-apply`

`$ make frontend-build`

`$ make frontend-deploy`