variable "bucket_domain_name" {
    type = string
    default = ""
}
variable "s3_origin_id" {
    type = string
    default = ""
}
variable "s3_origin_access_identity" {
    type = string
    default = null
}
variable "enabled" {
    type = bool
    default = false
}
variable "ipv6_enabled" {
    type = bool
    default = false
}
variable "comment" {
    type = string
    default = "cdn"
}
variable "default_root_object" {
    type = string
    default = "index.html"
}
variable "logging_cookies" {
    type = bool
    default = true
}
variable "logging_bucket" {
    type = string
    default = ""
}
variable "logging_bucket_prefix" {
    type = string
    default = null
}
variable "aliases" {
    type = list(string)
    default = []
}
variable "allowed_methods" {
    type = list(string)
    default = ["GET"]
}
variable "cached_methods" {
    type = list(string)
    default = ["GET"]
}
variable "forwarded_query_string" {
    type = string
    default = null
}
variable "forwarded_cookies" {
    type = string
    default = "none"
}
variable "viewer_protocol_policy" {
    type = string
    default = null
}
variable "min_ttl" {
    type = number
    default = 600
}
variable "default_ttl" {
    type = number
    default = 600
}
variable "max_ttl" {
    type = number
    default = 86400
}
variable "price_class" {
    type = string
    default = "Priceclass_200"
}
variable "geo_restriction_type" {
    type = string
    default = "none"
}
variable "geo_restriction_locations" {
    type = list(string)
    default = null
}
variable "tags" {
    type = map(string)
    default = {}
}
variable "cloudfront_default_certificate" {
    type = bool
    default = true
}
variable "oai_comment" {
    type = string
    default = ""
}
variable "cloudfront_certificate_arn" {
    type = string
    default = ""
}
variable "cloudfront_minimum_protocol_version" {
    type = string
    default = "TLSv1"
}
variable "cloudfront_ssl_support_method" {
    type = string
    default = "sni-only"
}

variable "oai_path" {
    type = string
    default = ""
}

variable "error_404_caching_min_ttl" {
    type = number
    default = 600
}
variable "error_404_code" {
    type = number
    default = 404
}
variable "error_404_response_code" {
    type = number
    default = 200
}
variable "error_404_response_page_path" {
    type = string
    default = "/index.html"
}

variable "error_403_caching_min_ttl" {
    type = number
    default = 600
}
variable "error_403_code" {
    type = number
    default = 404
}
variable "error_403_response_code" {
    type = number
    default = 200
}
variable "error_403_response_page_path" {
    type = string
    default = "/index.html"
}

variable "uploads_bucket_domain_name" {
    type = string
    default = ""
}
variable "uploads_s3_origin_id" {
    type = string
    default = ""
}