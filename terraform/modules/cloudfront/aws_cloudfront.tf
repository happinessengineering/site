resource "aws_cloudfront_distribution" "s3_distribution" {
  
  ## frontend
  origin {
    domain_name = var.bucket_domain_name
    origin_id   = var.s3_origin_id

    s3_origin_config {
      origin_access_identity = var.oai_path
    }
  }

  enabled             = var.enabled
  is_ipv6_enabled     = var.ipv6_enabled
  comment             = var.comment
  default_root_object = var.default_root_object

  logging_config {
    include_cookies = var.logging_cookies
    bucket          = var.logging_bucket
    prefix          = var.logging_bucket_prefix
  }

  aliases = var.aliases

  default_cache_behavior {
    allowed_methods  = var.allowed_methods
    cached_methods   = var.cached_methods
    target_origin_id = var.s3_origin_id

    forwarded_values {
      query_string = var.forwarded_query_string

      cookies {
        forward = var.forwarded_cookies
      }
    }

    viewer_protocol_policy = var.viewer_protocol_policy
    min_ttl                = var.min_ttl
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  custom_error_response {
    error_caching_min_ttl = var.error_404_caching_min_ttl
    error_code            = var.error_404_code
    response_code         = var.error_404_response_code
    response_page_path    = var.error_404_response_page_path
  }

  custom_error_response {
    error_caching_min_ttl = var.error_403_caching_min_ttl
    error_code            = var.error_403_code
    response_code         = var.error_403_response_code
    response_page_path    = var.error_403_response_page_path
  }

  price_class = var.price_class

  restrictions {
    geo_restriction {
      restriction_type = var.geo_restriction_type
      locations        = var.geo_restriction_locations
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = var.cloudfront_default_certificate
    acm_certificate_arn            = var.cloudfront_certificate_arn
    minimum_protocol_version       = var.cloudfront_minimum_protocol_version
    ssl_support_method             = var.cloudfront_ssl_support_method
  }
}