variable "environment" {
    type = string
    default = "development"
}

variable "maintainer" {
    type = string
    default = "admin"
}

variable "project" {
    type = string
    default = "terraform-project"
}

variable "region" {
    type = string
    default = "us-east-1"
}

variable "run_always" {
    type = string
    default = "false"
}