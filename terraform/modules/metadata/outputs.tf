output "tags" {
  value = {
    "account_id"  = data.aws_caller_identity.current.account_id,
    "environment" = var.environment,
    "maintainer"  = var.maintainer,
    "project"     = var.project,
    "region"      = var.region,
    "run_always"  = var.run_always,
    "unique_id"   = random_id.unique_id.b64_url
  }
}