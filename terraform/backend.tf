
provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    key            = "he-static-site/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform_locks"
  }

    required_version = "> 0.15.0"

  required_providers {
    aws = {
      version = "3.39.0"
    }
    random = {
      version = "3.1.0"
    }
    template = {
      version = "2.2.0"
    }
  }

}

