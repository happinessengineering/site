resource "aws_route53_zone" "happiness-dot-engineering" {
  name = "happiness.engineering"
}

resource "aws_route53_record" "happiness-dot-engineering" {
  zone_id = aws_route53_zone.happiness-dot-engineering.zone_id
  name    = "happiness.engineering"
  type    = "A"

  alias {
    name                   = module.cdn.domain_name
    zone_id                = module.cdn.zone_id
    evaluate_target_health = true
  }

}