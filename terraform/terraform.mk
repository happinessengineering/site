
.PHONY: default
default:

PWD=$(shell pwd)

ifndef DEPLOY_ENV
  DEPLOY_ENV := development
endif

tf-init:
	cd terraform; \
	  terraform init -backend-config=bucket=happinessengineering-terraform-state

tf-workspace:
	cd terraform; \
	  terraform workspace new $(DEPLOY_ENV)

tf-plan: ## Plan the terraform
tf-plan:
	cd terraform; \
	  terraform workspace select $(DEPLOY_ENV) && \
	  terraform plan -var-file="../env/terraform/$(DEPLOY_ENV).tfvars"

tf-apply: ## Plan and apply the terraform
tf-apply:
	cd terraform; \
	  terraform workspace select $(DEPLOY_ENV) && \
	  terraform plan -out=planfile -var-file="../env/terraform/$(DEPLOY_ENV).tfvars" && \
	  terraform apply planfile && \
	  rm planfile

tf-destroy: ## Destroy the terraform
tf-destroy: check
	cd terraform; \
	  terraform workspace select $(DEPLOY_ENV) && \
	  terraform destroy -var-file="../env/terraform/$(DEPLOY_ENV).tfvars" --force

