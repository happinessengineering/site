module "metadata" {
  source = "./modules/metadata"

  project =    "happiness-dot-engineering"
  maintainer = "happinessbot@happiness.engineering"
  environment = terraform.workspace
}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "happiness-dot-engineering" 
}

data "template_file" "bucket_policy" {
  template = file("policy.json")
  vars = {
    oai = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
  }
}

resource "aws_s3_bucket" "frontend" {
  bucket = "happiness.engineering"
  policy = data.template_file.bucket_policy.rendered
}

resource "aws_s3_bucket_public_access_block" "frontend" {
  bucket = aws_s3_bucket.frontend.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

}

resource "aws_acm_certificate" "cert" {
  domain_name               = "*.happiness.engineering"
  subject_alternative_names = ["happiness.engineering"]
  validation_method         = "DNS"

  tags = {
    Name = "happiness dot engineering"
  }

  lifecycle {
    create_before_destroy = true
  }
}

module "cdn" {
  source = "./modules/cloudfront"

  bucket_domain_name                  = aws_s3_bucket.frontend.bucket_regional_domain_name
  s3_origin_id                        = "happinessengineering"
  oai_path                            = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
  enabled                             = true
  ipv6_enabled                        = false
  comment                             = "he cdn"
  default_root_object                 = "index.html"
  logging_cookies                     = true
  logging_bucket                      = aws_s3_bucket.frontend.bucket_domain_name
  logging_bucket_prefix               = "logs"
  aliases                             = ["happiness.engineering"]
  allowed_methods                     = ["GET", "HEAD", "POST", "PUT", "PATCH", "OPTIONS", "DELETE"]
  cached_methods                      = ["GET", "HEAD"]
  forwarded_query_string              = false
  forwarded_cookies                   = "none"
  viewer_protocol_policy              = "redirect-to-https"
  min_ttl                             = 0
  default_ttl                         = 3600
  max_ttl                             = 86400
  price_class                         = "PriceClass_200"
  geo_restriction_type                = "none"
  geo_restriction_locations           = null
  cloudfront_default_certificate      = false
  cloudfront_certificate_arn          = aws_acm_certificate.cert.arn
  cloudfront_minimum_protocol_version = "TLSv1.2_2019"
  cloudfront_ssl_support_method       ="sni-only"

  error_404_caching_min_ttl           = 300
  error_404_code                      = 404
  error_404_response_code             = 200
  error_404_response_page_path        = "/index.html"

  error_403_caching_min_ttl           = 300
  error_403_code                      = 403
  error_403_response_code             = 200
  error_403_response_page_path        = "/index.html"
}